import React, {Component} from 'react'

import Section from '../components/section'

import {getComponentSections} from '../services/component-list-service'

export default class IndexPage extends Component {

  constructor(props) {
    super(props);

    this.state = {
      components: []
    };
  }

  componentWillMount() {
    getComponentSections().then(components => {
      this.setState({components: components});
    });
  }

  render() {

    if(!this.state.components) {
      return null;
    }
    
    let components = [];
    let collapsed = false;

    for(let section in this.state.components) {
      components.push(
        <Section name={section} collapsed={collapsed} section={this.state.components[section]} key={section} />
      );
      collapsed = true;
    } 

    return (
      <div>
          {components}
      </div>
    );
    return null;
  }
}