import React, {Component} from 'react'

import {getComponentSections} from '../services/component-list-service';

export default class Navigation extends Component {
    constructor(props) {
        super(props);

        this.state = {
        };
    }

    componentWillMount() {
        getComponentSections()
        .then(sections => {
            this.setState({components: sections});
        }, error => {console.error(error);});
    }

    render() {
        if(!this.state.components) { return null; }

        let navList = [];

        for(let section in this.state.components) {
            let components = this.state.components[section];
            let listItems = components.map((c, i) => {
                let id = `${section}_${c.replace(" ", "_")}`;
                let hash = `#${id}`;
                return (<li key={i}><a href={hash}>{c}</a></li>);
            });

            let sectionId = `#${section}`;
            navList.push(
                <div key={sectionId}>
                    <a href={sectionId}>{section}</a>
                    <ul>
                        {listItems}
                    </ul>
                </div>
            );
        }

        return (
            <div>
                {navList}
            </div>
        );
    }
}