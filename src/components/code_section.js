import React, {Component} from 'react'
import './section.css'

import Markdown from 'remarkable'
import hljs from 'highlight.js'
import Highlight from 'react-highlight.js'

export default class CodeSection extends Component {
    constructor(props) {
        super(props);

        this.state = {
            name: props.name,
            section: props.section,
            file_root: "/static/design",
            notes: "",
            html: ""
        };
    }

    componentWillMount() {

        this.getMarkdown(this.state.section,this.state.name).then(data => {
            let md = new Markdown({
                html:true,
                highlight: (str, lang) => {
                    try {
                        return hljs.highlight("xml", str).value;
                    }catch(err) {console.error(err)}

                    return "";
                }
            });
            let notes = md.render(data);
            this.setState({
                notes: notes
            });
        });

        this.getHtml(this.state.section, this.state.name).then(data => {
            this.setState({
                html: data
            });
        });
    }

    getHtml (section, name) {
        return fetch(`${this.state.file_root}/${name}/${section.replace(" ", "_")}.html`, {
            method: 'GET',
            headers: {
                'Accept': 'application/html',
                'Content-Type': 'application/html'
            }
        }).then(response => {
            const promise = new Promise((resolve, reject) => {
                if (!response.ok) {
                    reject("no data");
                }

                response.text().then(data => {
                    resolve(data);
                }, error => reject(error));
            });
            return promise;
        }, error => console.error(error));
    }

    getMarkdown (section, name) {
        return fetch(`${this.state.file_root}/${name}/${section.replace(" ", "_")}.md`, {
            method: 'GET',
            headers: {
                'Accept': 'text/markdown',
                'Content-Type': 'text/markdown'
            }
        }).then(response => {
            const promise = new Promise((resolve, reject) => {
                if (!response.ok) {
                    reject("no data");
                }

                response.text().then(data => {
                    resolve(data);
                }, error => reject(error));
            });
            return promise;
        }, error => console.error(error));
    }

    render() {
        let sections = [];

        if(this.state.html !== "")
        {
            sections.push(
                <p key="html" dangerouslySetInnerHTML={{__html: this.state.html}} />
            );

            sections.push(
                <Highlight language="xml" key="html_code">
                    {this.state.html}
                </Highlight>
            );
        }
    
        if(this.state.notes !== "")
        {
            sections.push(
                <div key="note">
                    <h3>Notes:</h3>
                    <p dangerouslySetInnerHTML={{__html: this.state.notes}} />
                </div>
            );
        }

        return (
         <div id = {`${this.state.name}_${this.state.section.replace(" ", "_")}`}
            className="code_section">
            <h2>{this.state.section}</h2>
             {sections}
         </div>
        );
    }
}