import React, {Component} from 'react'
import CodeSection from './code_section'
import './section.css';

export default class Section extends Component {
    constructor(props) {
        super(props);
        this.state = {
            name: props.name,
            section: props.section,
            collapsed: props.collapsed
        };

        this.onCollapsedChanged = this.onCollapsedChanged.bind(this);
    }

    onCollapsedChanged() {
        console.log("Collapse changed");
        this.setState({collapsed: !this.state.collapsed});
    }

    render() {
        let sections = this.state.section.map((s, i) => {
            return (<CodeSection key={i} name={this.state.name} section={s} />);
        });

        const activeStyle = this.state.collapsed ? "" : " active";
        const css = `collapsible${activeStyle} section`;
        const style = {
            display: this.state.collapsed ? "none" : "block"
        };

        return (
            <section className={css} id={this.state.name.replace(" ", "_")}>
                <h1 className="header" onClick={(e) => this.onCollapsedChanged(e)}>{this.state.name}</h1>
                <div className="content" style={style}>
                {sections}
                </div>
            </section>
        );
    }
}