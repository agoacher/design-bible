import React from 'react'
import PropTypes from 'prop-types'
import Helmet from 'react-helmet'

import Header from '../components/header'
import './index.css'

import Navigation from '../components/navigation'

const Layout = ({ children, data }) => (
  <div className="container">
    <Helmet
      title={data.site.siteMetadata.title}
    />
    <header>
      <Header siteTitle={data.site.siteMetadata.title} />
    </header>
    <nav>
      <Navigation />
    </nav>
    <main>
      {children()}
    </main>
    <footer>
      <p>Some footer content</p>
    </footer>
  </div>
)

Layout.propTypes = {
  children: PropTypes.func,
}

export default Layout

export const query = graphql`
  query SiteTitleQuery {
    site {
      siteMetadata {
        title
      }
    }
  }
`
