const components_file = "/static/design/components.json";

const get_json = (url) => {
    return fetch(url, {
        method: 'GET',
        headers:{
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        }
    }).then(response => {
        const promise = new Promise((resolve,reject) => {
            if(!response.ok){
                reject('no data');
            }

            response.json().then(data => {
                resolve(data);
            }, error => reject(error));
        });

        return promise;
    }, error => console.log(error));
}

const getComponentSections = () => {
    return get_json(components_file).then(data => {
        if(data && data.files) {
            return data.files;
        }

        return null;
    });
}

export {getComponentSections};