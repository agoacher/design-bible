module.exports = {
  siteMetadata: {
    title: 'Design Bible',
  },
  plugins: ['gatsby-plugin-react-helmet'],
}
