Table can be used in Compliance Centre using the angular control
```
<dri-table
    opts="Model.options"></dri-table>
```
Where opts is an array of type ```DriTableOption```

Table can be used in a react project with the react component

```
<DriTable
    opts={options} />
```

Where opts is an array of type ```DriTableOption```